FROM openjdk:8-jdk-alpine
ENV db_url='jdbc:mysql://${DB_SERVICE_HOST}:${DB_SERVICE_PORT}/assignment?allowPublicKeyRetrieval=true&createDatabaseIfNotExist=true'
ENV db_user=''
ENV db_password=''
ENV db_profile='h2'
COPY --chown=nobody target/*.jar  /tmp/app.jar
USER nobody
CMD  java -jar -Dspring.profiles.active=${db_profile} -Dspring.datasource.password=${db_password} -Dspring.datasource.username=${db_user} -Dspring.datasource.url=${db_url} /tmp/app.jar

